﻿namespace KSU.Data
{
    public class MemberService
    {
        private static List<Member>? memberList;
        private static int id = 1;

        public List<Member> getMemberList()
        {
            memberList = new List<Member> { new Member { id = id, name = "name", password = "password" } };
            return memberList;
        }

        public void save(Member member)
        {
            id++;
            member.id = id;
            memberList.Add(member);

        }

        public void deleteMember(Member member)
        {
            memberList.Remove(member);
        }

        public void update(Member newMember)
        {
            foreach (var member in memberList)
            {
                if (string.Equals(newMember.id, member.id))
                {
                    member.name = newMember.name;
                    member.password = newMember.password;
                }
            }
        }




    }
}
