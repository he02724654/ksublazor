﻿window.bootstrapModal = {};
window.bootstrapModal.openedModal = null;

window.bootstrapModal.showModal = function (id) {

    if (window.bootstrapModal.openedModal != null) {
        window.bootstrapModal.hideModal();
    }

    window.bootstrapModal.openedModal = new bootstrap.Modal(document.getElementById(id));
    window.bootstrapModal.openedModal.show();
}

window.bootstrapModal.hideModal = function () {
    if (window.bootstrapModal.openedModal == null) {
        return;
    }

    window.bootstrapModal.openedModal.hide();
    window.bootstrapModal.openedModal = null;
}
